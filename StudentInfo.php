<?php


class StudentInfo
{
    public $student_id="";
    public $student_name="";
    public $student_cgpa;

    public function StudentInfo()
    {
        echo "Hello World"."<br>";
    }
    public function getStudentId()
    {
        return $this->student_id;
    }
    public function getStudentName()
    {
        return $this->student_name;
    }
    public function getStudentCgpa()
    {
        return $this->cgpa;
    }

    public function setStudentId($student_id)
    {
         $this->student_id=$student_id;
    }
    public function setStudentName($student_name)
    {
        $this->student_name=$student_name;
    }
    public function setStudentCgpa($student_cgpa)
    {
        $this->cgpa=$student_cgpa;
    }
    function __destruct()
    {
        echo "Good Bye";
    }
    static function getStaticMethod($msg)
    {
        echo " $msg <br>";
    }

}
$rahim=new StudentInfo();
$kahim=new StudentInfo();

$rahim->setStudentId(132820);
$rahim->setStudentName("Rahim");
$rahim->setStudentCgpa(4.75);

$id=$rahim->getStudentId();
$name=$rahim->getStudentName();
$cgpa=$rahim->getStudentCgpa();

echo "Id: ". $id." Name: ".$name." CGPA: ".$cgpa."<br>";

StudentInfo::getStaticMethod("Data has been inseted");
StudentInfo::getStaticMethod("Data has been deleted");
